<?php

namespace App\Http\Controllers\Admin;

use App\EmailNotificationSetting;
use App\Helper\Reply;
use App\SlackSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlackSettingController extends AdminBaseController
{
    public function __construct() {
        parent::__construct();
        $this->pageTitle = __('app.menu.slackSettings');
        $this->pageIcon = 'fa fa-slack';
    }

    public function index(){
        $this->emailSettings = EmailNotificationSetting::all();
        $this->slackSettings = SlackSetting::first();
        return view('admin.slack-settings.index', $this->data);
    }

    public function update(Request $request, $id){
        $setting = SlackSetting::find($id);
        $setting->slack_webhook = $request->slack_webhook;

        if ($request->hasFile('slack_logo')) {
            $setting->slack_logo = $request->slack_logo->hashName();
            $request->slack_logo->store('public/slack-logo');
        }
        $setting->save();

        return Reply::redirect(route('admin.slack-settings.index'), __('messages.settingsUpdated'));
    }

    public function updateSlackNotification(Request $request){
        $setting = EmailNotificationSetting::find($request->id);
        $setting->send_slack = $request->send_slack;
        $setting->save();

        return Reply::success(__('messages.settingsUpdated'));
    }
}
