<?php

namespace App\Http\Controllers\Admin;

use App\Currency;
use App\Helper\Reply;
use App\Http\Requests\Currency\StoreCurrency;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CurrencySettingController extends AdminBaseController
{
    public function __construct() {
        parent::__construct();
        $this->pageIcon = 'icon-settings';
        $this->pageTitle = __('app.menu.currencySettings');
    }

    public function index() {
        $this->currencies = Currency::all();
        return view('admin.currencies.index', $this->data);
    }

    public function create() {
        return view('admin.currencies.create', $this->data);
    }

    public function edit($id) {
        $this->currency = Currency::find($id);
        return view('admin.currencies.edit', $this->data);
    }

    public function store(StoreCurrency $request) {

        $currency = new Currency();
        $currency->currency_name = $request->currency_name;
        $currency->currency_symbol = $request->currency_symbol;
        $currency->currency_code = $request->currency_code;

        // get exchange rate
        $client = new Client();
        $res = $client->request('GET', 'http://free.currencyconverterapi.com/api/v3/convert?q='.$this->global->currency->currency_code.'_'.$currency->currency_code);
        $conversionRate = $res->getBody();
        $conversionRate = json_decode($conversionRate, true);

        $currency->exchange_rate = $conversionRate['results'][$this->global->currency->currency_code.'_'.$currency->currency_code]['val'];
        $currency->save();

        return Reply::redirect(route('admin.currency.edit', $currency->id), __('messages.currencyAdded'));
    }

    public function update(StoreCurrency $request, $id) {
        $currency = Currency::find($id);
        $currency->currency_name = $request->currency_name;
        $currency->currency_symbol = $request->currency_symbol;
        $currency->currency_code = $request->currency_code;
        $currency->exchange_rate = $request->exchange_rate;
        $currency->save();

        return Reply::success(__('messages.currencyUpdated'));
    }

    public function destroy($id) {
        if($this->global->currency_id == $id){
           return Reply::error(__('modules.currencySettings.cantDeleteDefault'));
        }
        Currency::destroy($id);
        return Reply::success(__('messages.currencyDeleted'));
    }

    public function exchangeRate($currency){
        // get exchange rate
        $client = new Client();
        $res = $client->request('GET', 'http://free.currencyconverterapi.com/api/v3/convert?q='.$this->global->currency->currency_code.'_'.$currency);
        $conversionRate = $res->getBody();
        $conversionRate = json_decode($conversionRate, true);

        return $conversionRate['results'][$this->global->currency->currency_code.'_'.$currency]['val'];
    }

    public function updateExchangeRate(){

        $currencies = Currency::all();

        foreach($currencies as $currency){

            $currency = Currency::find($currency->id);

            // get exchange rate
            $client = new Client();
            $res = $client->request('GET', 'http://free.currencyconverterapi.com/api/v3/convert?q='.$this->global->currency->currency_code.'_'.$currency->currency_code);
            $conversionRate = $res->getBody();
            $conversionRate = json_decode($conversionRate, true);

            $currency->exchange_rate = $conversionRate['results'][$this->global->currency->currency_code.'_'.$currency->currency_code]['val'];
            $currency->save();
        }


        return Reply::success(__('messages.exchangeRateUpdateSuccess'));
    }
}
