@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ $pageTitle }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">{{ $pageTitle }}</div>

                <div class="vtabs customvtab m-t-10">

                    @include('sections.admin_setting_menu')

                    <div class="tab-content">
                        <div id="vhome3" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="table-responsive">

                                        <table class="table table-bordered">
                                            <thead>
                                            <th>@lang('modules.update.systemDetails')</th>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>Worksuite Version <span
                                                            class="pull-right">{{ $worksuiteVersion }}</span></td>
                                            </tr>
                                            <tr>
                                                <td>Laravel Version <span
                                                            class="pull-right">{{ $laravelVersion }}</span></td>
                                            </tr>
                                            <tr>
                                                <td>PHP Version <span class="pull-right">{{ phpversion() }}</span></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <h4>@lang('app.update') Worksuite</h4>

                                    <p>
                                        <label class="label label-danger">@lang('app.note')
                                            :</label> @lang('modules.update.fileReplaceAlert')
                                    </p>

                                </div>
                            </div>

                            <hr>
                            <!--row-->
                            <div class="row">
                                <div class="col-md-12">
                                        <h4 class="box-title" id="structure">Update Log</h4>
                                        <pre>
    <p>
        <strong>Worksuite Updates Log</strong>
        ├──
        │   └── <strong>Version 1.2</strong>
        │       └── Multi Language
        │       └── CSV Data Export
        │       └── Theme Settings
        │           └── Ability to change login background image
        │       └── Roles Management
        │           └── Added new role Project Admin
        │
        │   └── <strong>Version 1.3</strong>
        │       └── Added multiple taxes in invoices
        │       └── Send estimates/quotations to clients
        │       └── Added PayPal payment gateway to pay invoices
        │       └── Added new section payments in admin
        │
        │   └── <strong>Version 1.4</strong>
        │       └── Expense Management
        │       └── Invoice templates
        │       └── Expense vs Income report
        │       └── Slack Integration
        │       └── Client Section Made Multi Language
        │       └── Payment Settings
        │           └── Allow PayPal to make recurring payments
        │           └── Added Stripe payment gateway
        │
        │   └── <strong>Version 1.5</strong>
        │       └── Ticket Management
        │       └── Admin chat with employees module
        │       └── Notification icon in sidebar
        │
        │   └── <strong>Version 1.6</strong>
        │       └── Employee Attendance
        │       └── Sticky notes redesigned
        │       └── Issue management module removed as ticket management module is added.
        └──
    </p>
                                        </pre>
                                </div>
                            </div>
                            <!--/row-->

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>
    <!-- .row -->

@endsection

