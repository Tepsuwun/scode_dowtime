@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ $pageTitle }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/morrisjs/morris.css') }}"><!--Owl carousel CSS -->
<link rel="stylesheet" href="{{ asset('plugins/bower_components/owl.carousel/owl.carousel.min.css') }}"><!--Owl carousel CSS -->
<link rel="stylesheet" href="{{ asset('plugins/bower_components/owl.carousel/owl.theme.default.css') }}"><!--Owl carousel CSS -->

<style>
    .col-in{
        padding: 0 20px !important;
    }
</style>
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="white-box">
                <div class="row row-in">
                    <div class="col-lg-3 col-sm-6 row-in-br">
                        <div class="col-in row">
                            <h3 class="box-title">@lang('modules.dashboard.totalClients')</h3>
                            <ul class="list-inline two-part">
                                <li><i class="icon-user text-success"></i></li>
                                <li class="text-right"><span class="counter">{{ $counts->totalClients }}</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                        <div class="col-in row">
                            <h3 class="box-title">@lang('modules.dashboard.totalEmployees')</h3>
                            <ul class="list-inline two-part">
                                <li><i class="icon-people text-warning"></i></li>
                                <li class="text-right"><span class="counter">{{ $counts->totalEmployees }}</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6  row-in-br">
                        <div class="col-in row">
                            <h3 class="box-title">@lang('modules.dashboard.totalProjects')</h3>
                            <ul class="list-inline two-part">
                                <li><i class="icon-layers text-danger"></i></li>
                                <li class="text-right"><span class="counter">{{ $counts->totalProjects }}</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 b-0">
                        <div class="col-in row">
                            <h3 class="box-title">@lang('modules.dashboard.totalPaidInvoices')</h3>
                            <ul class="list-inline two-part">
                                <li><i class="ti-receipt text-inverse"></i></li>
                                <li class="text-right"><span class="counter">{{ $counts->totalPaidInvoices }}</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="row row-in">
                    <div class="col-md-3 col-sm-12 row-in-br">
                        <div class="col-in row">
                            <h3 class="box-title">@lang('modules.dashboard.totalHoursLogged')</h3>
                            <ul class="list-inline two-part">
                                <li><i class="icon-clock text-info"></i></li>
                                <li class="text-right"><span class="counter">{{ floor($counts->totalHoursLogged) }}</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12  row-in-br  b-r-none">
                        <div class="col-in row">
                            <h3 class="box-title">@lang('modules.dashboard.totalPendingTasks')</h3>
                            <ul class="list-inline two-part">
                                <li><i class="ti-alert text-warning"></i></li>
                                <li class="text-right"><span class="counter">{{ $counts->totalPendingTasks }}</span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-12 row-in-br ">
                        <div class="col-in row">
                            <h3 class="box-title">@lang('modules.dashboard.totalCompletedTasks')</h3>
                            <ul class="list-inline two-part">
                                <li><i class="ti-check-box text-success"></i></li>
                                <li class="text-right"><span class="counter">{{ $counts->totalCompletedTasks }}</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12  b-0">
                        <div class="col-in row">
                            <h3 class="box-title">@lang('modules.dashboard.totalTodayAttendance')</h3>
                            <ul class="list-inline two-part">
                                <li><i class="fa fa-percent text-danger"></i></li>
                                <li class="text-right"><span class="counter">@if($counts->totalEmployees > 0){{ round((($counts->totalTodayAttendance/$counts->totalEmployees)*100), 2) }}@else 0 @endif</span></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div class="row row-in">
                            <div class="col-md-6 col-sm-12 row-in-br">
                                <div class="col-in row">
                                    <h3 class="box-title">@lang('modules.tickets.totalResolvedTickets')</h3>
                                    <ul class="list-inline two-part">
                                        <li><i class="ti-ticket text-success"></i></li>
                                        <li class="text-right"><span class="counter">{{ floor($counts->totalResolvedTickets) }}</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12  b-0">
                                <div class="col-in row">
                                    <h3 class="box-title">Unresolved Tickets</h3>
                                    <ul class="list-inline two-part">
                                        <li><i class="ti-ticket text-danger"></i></li>
                                        <li class="text-right"><span class="counter">{{ $counts->totalUnResolvedTickets }}</span></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-xs-12">
                    <div class="bg-theme m-b-15">
                        <div class="row weather p-20">
                            @if(is_null($global->latitude))
                                <div class="col-md-12">
                                    <a href="{{ route('admin.settings.index') }}" class="text-white"><i class="ti-location-pin"></i>
                                        <br>
                                        @lang('modules.dashboard.weatherSetLocation')</a>
                                </div>
                            @else
                                <div class="col-md-6 col-xs-6 col-lg-6 col-sm-6 m-t-40">
                                    <h3>&nbsp;</h3>
                                    <h1>{{ ceil($weather['currently']['temperature']) }}<sup>°C</sup></h1>
                                </div>
                                <div class="col-md-6 col-xs-6 col-lg-6 col-sm-6 text-right"> <canvas class="{{ $weather['currently']['icon'] }}" width="45" height="45"></canvas><br/>
                                    <br/>
                                    <b class="text-white">{{ $weather['currently']['summary'] }}</b>
                                    <p class="w-title-sub">{{ \Carbon\Carbon::createFromTimestamp($weather['currently']['time'])->timezone($global->timezone)->format('F d') }}</p>
                                </div>
                                <div class="col-md-12">
                                    <p class="text-white">
                                        {{ $weather['hourly']['summary'] }}
                                    </p>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-xs-12">
                    <div class="bg-theme-dark m-b-15">
                        <div id="myCarouse2" class="carousel vcarousel slide p-20">
                            <h4 class="text-white p-t-0 p-b-0">@lang('modules.projects.clientFeedback')</h4>
                            <!-- Carousel items -->
                            <div class="carousel-inner ">
                                @forelse($feedbacks as $key=>$feedback)
                                    <div class="@if($key == 0) active @endif item">
                                        <h4 class="text-white">{!! substr($feedback->feedback,0,70).'...' !!}</h4>
                                        <div class="twi-user">
                                            {!!  ($feedback->client->image) ? '<img src="'.asset('storage/avatar/'.$feedback->client->image).'"
                                                                        alt="user" class="img-circle img-responsive pull-left">' : '<img src="'.asset('default-profile-2.png').'"
                                                                        alt="user" class="img-circle img-responsive pull-left">' !!}
                                            <h4 class="text-white m-b-0">{{ ucwords($feedback->client->name) }}</h4>
                                            <p class="text-white">{{ ucwords($feedback->project_name) }}</p>
                                        </div>
                                    </div>
                                @empty
                                    <div class="active item">
                                        <h3 class="text-white">@lang('messages.noFeedbackReceived')</h3>
                                    </div>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-xs-12">
                    <div class="white-box">
                        <h3 class="box-title">@lang('modules.dashboard.recentEarnings')</h3>
                        <ul class="list-inline text-right">
                            <li>
                                <h5><i class="fa fa-circle m-r-5" style="color: #e20b0b;"></i>Earning</h5>
                            </li>
                        </ul>
                        <div id="morris-area-chart" style="height: 340px;"></div>
                        <h6 style="line-height: 2em;"><span class=" label label-danger">@lang('app.note'):</span> @lang('messages.earningChartNote') <a href="{{ route('admin.settings.index') }}"><i class="fa fa-arrow-right"></i></a></h6>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- .row -->

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('modules.dashboard.overdueTasks')</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <ul class="list-task list-group" data-role="tasklist">
                            <li class="list-group-item" data-role="task">
                                <strong>@lang('app.title')</strong> <span
                                        class="pull-right"><strong>@lang('modules.dashboard.dueDate')</strong></span>
                            </li>
                            @forelse($pendingTasks as $key=>$task)
                                <li class="list-group-item" data-role="task">
                                    {{ ($key+1).'. '.ucfirst($task->heading) }} <a href="{{ route('admin.projects.show', $task->project_id) }}" class="text-danger">{{ ucwords($task->project->project_name) }}</a> <label
                                            class="label label-danger pull-right">{{ $task->due_date->format('d M') }}</label>
                                </li>
                            @empty
                                <li class="list-group-item" data-role="task">
                                    @lang("messages.noOpenTasks")
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('modules.dashboard.pendingClientIssues')</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <ul class="list-task list-group" data-role="tasklist">
                            @forelse($pendingIssues as $key=>$issue)
                                <li class="list-group-item" data-role="task">
                                    {{ ($key+1).'. '.ucfirst($issue->description) }} <a href="{{ route('admin.projects.show', $issue->project_id) }}" class="text-danger">{{ ucwords($issue->project->project_name) }}</a>
                                </li>
                            @empty
                                <li class="list-group-item" data-role="task">
                                    @lang("messages.noOpenIssues")
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row" >

        <div class="col-md-6" id="section-line-1">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('modules.dashboard.projectActivityTimeline')</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="steamline">
                            @foreach($projectActivities as $activ)
                                <div class="sl-item">
                                    <div class="sl-left"><i class="fa fa-circle text-info"></i>
                                    </div>
                                    <div class="sl-right">
                                        <div><h6><a href="{{ route('admin.projects.show', $activ->project_id) }}" class="text-danger">{{ ucwords($activ->project->project_name) }}:</a> {{ $activ->activity }}</h6> <span class="sl-date">{{ $activ->created_at->diffForHumans() }}</span></div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('modules.dashboard.userActivityTimeline')</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="steamline">
                            @forelse($userActivities as $key=>$activity)
                                <div class="sl-item">
                                    <div class="sl-left">
                                        {!!  ($activity->user->image) ? '<img src="'.asset('storage/avatar/'.$activity->user->image).'"
                                                                    alt="user" class="img-circle">' : '<img src="'.asset('default-profile-2.png').'"
                                                                    alt="user" class="img-circle">' !!}
                                    </div>
                                    <div class="sl-right">
                                        <div class="m-l-40"><a href="{{ route('admin.employees.show', $activity->user_id) }}" class="text-success">{{ ucwords($activity->user->name) }}</a> <span  class="sl-date">{{ $activity->created_at->diffForHumans() }}</span>
                                            <p>{!! ucfirst($activity->activity) !!}</p>
                                        </div>
                                    </div>
                                </div>
                                @if(count($userActivities) > ($key+1))
                                    <hr>
                                @endif
                            @empty
                                <div>@lang("messages.noActivityByThisUser")</div>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>


@endsection


@push('footer-script')


<script src="{{ asset('plugins/bower_components/raphael/raphael-min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/morrisjs/morris.js') }}"></script>

<script src="{{ asset('plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>
<script src="{{ asset('plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>

<!-- jQuery for carousel -->
<script src="{{ asset('plugins/bower_components/owl.carousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/owl.carousel/owl.custom.js') }}"></script>

<!--weather icon -->
<script src="{{ asset('plugins/bower_components/skycons/skycons.js') }}"></script>

<script>
    $(document).ready(function () {
        var chartData = {!!  $chartData !!};
        function barChart() {

            Morris.Line({
                element: 'morris-area-chart',
                data: chartData,
                xkey: 'date',
                ykeys: ['total'],
                labels: ['Earning'],
                pointSize: 3,
                fillOpacity: 0,
                pointStrokeColors:['#e20b0b'],
                behaveLikeLine: true,
                gridLineColor: '#e0e0e0',
                lineWidth: 2,
                hideHover: 'auto',
                lineColors: ['#e20b0b'],
                resize: true

            });

        }

        barChart();

        $(".counter").counterUp({
            delay: 100,
            time: 1200
        });

        $('.vcarousel').carousel({
            interval: 3000
        })


        var icons = new Skycons({"color": "#ffffff"}),
                list  = [
                    "clear-day", "clear-night", "partly-cloudy-day",
                    "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                    "fog"
                ],
                i;
        for(i = list.length; i--; ) {
            var weatherType = list[i],
                    elements = document.getElementsByClassName( weatherType );
            for (e = elements.length; e--;){
                icons.set( elements[e], weatherType );
            }
        }
        icons.play();
    })

</script>
@endpush