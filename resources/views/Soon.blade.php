<!DOCTYPE html>
<html lang="en-US">

<head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-97417324-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-97417324-1');
</script>

	  	<!-- Meta -->
	  	<meta charset="utf-8">
      	<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale = 1.0, user-scalable=no">
      	
		<!-- Title -->
      	<title>Smart Coder - Coming Soon</title>
		
		<!-- Favicon 
		<link rel="shortcut icon" href="images/favicon.ico">
    -->
		<!-- Google web fonts -->
      	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
      	
      	<!-- Font awesome -->
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" />
	  
	  	<!-- Stylesheet -->
      	<link rel="stylesheet" type="text/css" href="{{ asset('soon/layout/style.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('soon/layout/media.css') }}">
		
		<!-- Color schema -->
		<link class="colors" rel="stylesheet" href="{{ asset('soon/layout/colors/green.css') }}" type="text/css">
		
		<!-- Animate -->
    	<link rel="stylesheet" href="{{ asset('soon/layout/plugins/cssanimation/animate.css') }}" type="text/css">
	  
      	<!-- Bootstrap -->
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		
		

	</head>

  	<body>

		<!-- Loader -->
		<div class="page-loader">
			<div class="progress">Loading...</div>
		</div>

		<!-- Header -->
		<header id="#top">

			
    		<div class="logo">
				<img src="{{ asset('soon/images/logo.png') }}" width="196" height="202" alt="" /> 
			</div>
            <div>
                <p style="font-family:Open Sans, sans-serif; font-size:60px;color:#24b7a4">&#10094;Smart<span style="color:red;">Coder<span style="color:#24b7a4;">/&#10095;</span></span></p>
            </div>
    		<h1>COMING SOON</h1>

			<p>
				Our website is under construction, we are working very hard to give you the best experience with this one.<br />
				You will love Softeek as much as we do. It will morph perfectly on your needs!
			</p>
         
         	<!-- Countdown -->
         	<div class="countdown"></div>
         
         	<!-- Mouse icon -->
         	<div class="mouse">
         		<a  href="#services">
         			<i class="fa fa-chevron-down"></i>
         		</a>
         	</div>

		</header>

		<!-- Services -->
		<section class="services" id="services">
		  	<div class="container">
				<div class="row">
				  	
					<div class="col-md-12">
						
						<div class="col-md-4" data-sr="wait 0.25s, then enter left and move 40px over 1s">
							<div class="icon">
								<i class="fa fa-laptop fa-fw"></i>
							</div>
							<h2>Brilliant Ideas</h2>
						</div> 
						 
						<div class="col-md-4" data-sr="wait 0.25s, then enter top and move 40px over 1s">
							<div class="icon">
								<i class="fa fa-code fa-fw"></i>
							</div>
							<h2>Clean Code</h2>
						</div> 
						 
						<div class="col-md-4" data-sr="wait 0.25s, then enter right and move 40px over 1s">
							<div class="icon">
								<i class="fa fa-bullhorn fa-fw"></i>
							</div>
							<h2>24/7 Support</h2>
						</div>
						 
					</div>
				 
			   </div>
			</div>
		</section>

		<!-- Contact form  -->
		<section class="contact">
  			<div class="container">
				<div class="row">
				  	
					<h2 data-sr="wait 0.5s, then enter top and move 40px over 1s">
						<span>CONTACT US</span>
					</h2>
					<!--
				  	<div class="col-md-12">
				  		<div id="note"></div>
						
						<div id="fields">
						
							<form id="ajax-contact-form">
						  		<input type="text" name="name" id="name" placeholder="Name" value="" data-sr="enter left move 25px, after 0.3s"/>
						  		<input type="email" name="email" id="email" placeholder="Email" value="" data-sr="enter right move 25px, after 0.3s"/>
						  		<textarea name="message" id="message" placeholder="Message"  data-sr="enter bottom move 25px, after 0.3s"></textarea>
						  		<input type="submit" name="submit" id="submit" value="Send message" data-sr="wait 0.8s, then enter top and move 40px after 0.3s"/>
							</form>
						
					 	</div>						
				  	</div>
					-->
			 	</div>
			</div>
		</section>
        

		<!-- Google Maps -->
		<section class="google-map">
			<div id="google-container"></div>
			<div id="zoom-in"></div>
			<div id="zoom-out"></div>
			
			<div class="contact-info">
				<div class="container">
				 	<div class="row">
				  		<div class="col-md-12">
					 		
							<address data-sr="wait 0.25s, then enter left and move 40px over 1s">
								<i class="fa fa-map-marker"></i> 6 October , EGYPT

							</address> 
							
					 		<div class="phone" data-sr="wait 0.25s, then enter top and move 40px over 1s">
								<i class="fa fa-mobile"></i> +2 010 1313 6099
							</div> 
					 		
							<div class="email" data-sr="wait 0.25s, then enter right and move 40px over 1s">
								<i class="fa fa-paper-plane"></i> <a href="mailto:info@smart-coder.com">info@smart-coder.com</a>
							</div> 
				  		
						</div>
				 	</div>
				</div>
			</div>			  
		</section>

		<!-- Footer -->
		<footer>
		  	<div class="container">
			 	<div class="row">
		
					<!-- Up button -->
					<a  href="#top" class="up-btn"><i class="fa fa-chevron-up"></i></a>
		  
		  			<!-- Social links -->
					<ul class="copyright" data-sr="wait 0.5s, then enter left and move 40px over 1s">
				 		Copyright © 2017 <a href="https://www.smart-coder.com" target="_blank">Smart Coder</a>
					</ul>
		
					<!-- Copyright  -->
			   		<div class="social-icon" data-sr="wait 0.5s, then enter right and move 40px over 1s">
						<!--<li><a href="#"><i class="fa fa-fw fa-youtube"></i></a></li> -->
				 		<!-- <li><a href="#"><i class="fa fa-fw fa-twitter"></i></a></li> -->
				 		<li><a href="https://www.facebook.com/SmartCoderCom/"><i class="fa fa-fw fa-facebook"></i></a></li>
				 		<!--<li><a href="#"><i class="fa fa-fw fa-dribbble"></i></a></li> -->
				 		<!--<li><a href="#"><i class="fa fa-fw fa-google-plus"></i></a></li> -->
					</div>
		
			 	</div>
		  	</div>
		</footer>

	</body>
	
	<!-- jQuery -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.js"></script>
	
	<!-- Plugins -->
	<script src="{{ asset('soon/layout/plugins/backstretch/jquery.backstretch.min.js') }}"></script>
	<script src="{{ asset('soon/layout/plugins/countdown/jquery.countdown.min.js') }}"></script>
	<script src="{{ asset('soon/layout/plugins/validate/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('soon/layout/plugins/scrollreveal/scrollreveal.js') }}"></script>
	<script src="{{ asset('soon/layout/plugins/ytplayer/jquery.mb.ytplayer.js') }}"></script>
	<script src="{{ asset('soon/layout/plugins/smoothscroll/smoothscroll.js') }}"></script>
	<script src="{{ asset('soon/layout/plugins/ajaxchimp/jquery.ajaxchimp.min.js') }}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTcGfc7PNk8SYWhNdIgfFuV03611YIbT4"></script>
	
	<!-- Main -->
	<script src="{{ asset('soon/layout/js/main.js') }}"></script>
	
	<script type="text/javascript">
		//Google Maps
		var arrMap = {
			"title":"6th October City, Giza Governorate , Egypt",
			"latitude":29.960429,
			"longitude":30.929331,
			"zoom":15,
			"marker":"{{ asset('soon/layout/images/map-marker-blue.png') }}",
			"color":"#16b6ea"
		}
	
		$(document).ready(function() {
			//Backstretch
			$("header").backstretch([		
				"{{ asset('soon/images/slide/2.jpg') }}", 
			], {duration:3000, fade:750});
		});
	</script>
	
	
</html>