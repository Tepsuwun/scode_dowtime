<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Role;

class RemoveProjectAdminRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Role::where('name', 'project_admin')->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $client = new Role();
        $client->name = 'project_admin';
        $client->display_name = 'Project Admin'; // optional
        $client->description = 'Project admin is allowed to manage all the projects in a company.'; // optional
        $client->save();
    }
}
