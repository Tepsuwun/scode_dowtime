<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\ModuleSetting;

class AddTicketModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Employee Modules
        $module = new ModuleSetting();
        $module->type = 'employee';
        $module->module_name = 'tickets';
        $module->status = 'active';
        $module->save();

        // Client Modules
        $module = new ModuleSetting();
        $module->type = 'client';
        $module->module_name = 'tickets';
        $module->status = 'active';
        $module->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
