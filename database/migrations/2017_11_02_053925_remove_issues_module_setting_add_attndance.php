<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\ModuleSetting;

class RemoveIssuesModuleSettingAddAttndance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ModuleSetting::where('module_name', 'issues')->where('type', 'client')->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $module = new ModuleSetting();
        $module->type = 'client';
        $module->module_name = 'issues';
        $module->status = 'active';
        $module->save();
    }
}
