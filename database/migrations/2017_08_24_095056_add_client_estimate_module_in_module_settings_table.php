<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClientEstimateModuleInModuleSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $module = new \App\ModuleSetting();
        $module->type = 'client';
        $module->module_name = 'estimates';
        $module->status = 'active';
        $module->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\ModuleSetting::where('type', 'client')
            ->where('module_name', 'estimates')
            ->delete();
    }
}
